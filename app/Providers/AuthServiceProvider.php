<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use App\Policies\QuestionsPolicy;
use App\Policies\AnswerPolicy;
use App\Answer;
use App\Question;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        // Register Your policy
        Question::class => QuestionsPolicy::class,
        Answer::class => AnswerPolicy::class
     ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //



        //write by me
        // Gate::define('update-question', function($user, $question){
        //     return $user->id === $question->user_id;
        // });
        // Gate::define('delete-question', function($user, $question){
        //     return $user->id === $question->user_id && $question->answers_count === 0;
        // });

    }
}
